import Vue from 'vue'
import Router from 'vue-router'
import DocumentList from '@/components/DocumentList'
import DocumentEdit from '@/components/DocumentEdit'
import DocumentCreate from '@/components/DocumentCreate'
import ApplicationStatus from '@/components/ApplicationStatus'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'document.list',
      component: DocumentList
    },
    {
      path: '/create',
      name: 'document.create',
      component: DocumentCreate
    },
    {
      path: '/edit/:id',
      name: 'document.edit',
      component: DocumentEdit
    },
    {
      path: '/application-status',
      name: 'application.status',
      component: ApplicationStatus
    }
  ]
})
