import { http } from '@/support/http'
import { parse } from '@/support/payloadParser'
import qs from 'qs'

export const getDocuments = (searchParams = {}) => {
  return http.get(`/document/list?${qs.stringify(searchParams)}`)
}

export const loadDocument = (id) => {
  return http.get(`/document/${id}`)
}

export const createDocument = (document) => {
  document.blacklisted = Number(document.blacklisted)
  return http.post(`/document`, parse(document))
}

export const updateDocument = (document) => {
  document.blacklisted = Number(document.blacklisted)
  return http.put(`/document/${document.id}`, parse(document))
}

export const deleteDocument = (id) => {
  return http.delete(`/document/${id}`)
}

export const getUptime = () => {
  return http.get('/status')
}
