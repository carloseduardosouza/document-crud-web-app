import 'url-search-params-polyfill'

export const parse = payload => {
  const params = new URLSearchParams() // eslint-disable-line no-undef

  if (typeof payload !== 'object') return null

  let keys = Object.keys(payload)
  const length = keys.length

  for (let i = 0; i < length; i += 1) {
    let value = payload[keys[i]]
    let key = keys[i]
    if (value instanceof Array) {
      value.forEach((arrValue) => {
        params.append(key + '[]', arrValue)
      })
    } else {
      value = String(value)

      if (value !== null && value !== 'null' && value !== 'undefined' && value !== undefined && value.length) {
        params.append(key, value)
      }
    }
  }

  return params
}

export const parseToFormData = payload => {
  const params = new FormData() // eslint-disable-line no-undef

  if (typeof payload !== 'object') return null

  let keys = Object.keys(payload)
  const length = keys.length

  for (let i = 0; i < length; i += 1) {
    let value = payload[keys[i]]
    let key = keys[i]
    value = String(value)

    if (value !== null && value !== 'undefined' && value !== undefined && value.length) {
      params.append(key, value)
    }
  }

  return params
}
