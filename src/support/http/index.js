import axios from 'axios'
import { API_URL } from '@/config'

export const http = axios.create({
  baseURL: API_URL
})

export default function install (Vue, { store, router }) {
  Object.defineProperty(Vue.prototype, '$http', {
    get () {
      return http
    }
  })
}
